# pastit

![version: 0.7.1](https://img.shields.io/badge/version-0.7.1-blue.svg?longCache=true&style=for-the-badge)
![bash langage](https://img.shields.io/badge/bash-4-brightgreen.svg?longCache=true&style=for-the-badge)
![license LPRAB / WTFPL](https://img.shields.io/badge/license-LPRAB%20%2F%20WTFPL-blue.svg?longCache=true&style=for-the-badge)


* un fichier texte est exporté sur _pastery.net_, avec coloration syntaxique correspondant au type détecté
* un fichier binaire de moins de 100ko est exporté sur _tranfer.sh_ pour être téléchargeable
  * temps de conservation non réglable de 14 jours
* un fichier binaire de plus de 100ko est exporté sur _dl.free.fr_ pour être téléchargeable ( A VENIR )
  * temps de conservation non réglage de 30 jours après le dernier téléchargement



## chargement et lancement du script:

```shell
wget -nv -O pastit https://framaclic.org/h/pastit
chmod +x pastit
```


## utilisation

```shell
./pastit LICENSE.md
```
```text
   ____           _   _ _ _    
  |  _ \ __ _ ___| |_( |_) |_  
  | |_) / _' / __| __|/| | __| 
  |  __/ (_| \__ \ |_  | | |_  
  |_|   \__,_|___/\__| |_|\__| 
        version 0.5.0 - 21/06/2018
  
  fichier markdown
  lien paste:    https://www.pastery.net/hctntk/  (valide pendant 7 jours)

```


## help

```shell
./pastit -h
```
```text
   ____           _   _ _ _    
  |  _ \ __ _ ___| |_( |_) |_  
  | |_) / _' / __| __|/| | __| 
  |  __/ (_| \__ \ |_  | | |_  
  |_|   \__,_|___/\__| |_|\__| 
        version 0.5.0 - 22/06/2018
  
  Usage:
    pastit [options] <file>
  
  Options:
    -h  , --help   : cette aide

    -i, --install  : installation (root requis)
    -r, --remove   : désinstallation (root requis)
    -u, --upgrade  : provoque le chargement et le remplacement du script installé (root requis)
    -us            : upgrade spécial du script en place (sans être installé)
    -v, --version  : version du script en ligne et en place 

          --debug-paste : affichage retour json de de l'export sur pastebin
          --dev    : une version de dev du script (si existante) est recherchée
    -t n, --time n : temps de conservation du paste, par défaut 7 jours

  plus d'infos: https://framaclic.org/h/doc-pastit"

```


## pastebins 

* [pastery.net](https://www.pastery.net/) pastebin
* [transfer.sh](https://transfer.sh/) pastebin avec capacité de fichiers binaires, mais fonctionnalités limitées
* [markdownshare.com](https://markdownshare.com/)


## sources

[framagit (gitlab) (framasoft service)](https://framagit.org/sdeb/pastit/blob/master/pastit)


## license

* [LPRAB / WTFPL](https://framagit.org/sdeb/pastit/blob/master/LICENSE.md)


![compteur](https://framaclic.org/h/pastit-gif)
