# pastit changelog

tags utilisés: added  changed  cosmetic  deprecated  fixed  new rewriting  removed  syncro  security
the date in format: YYYY-MM-DD

## [Unreleased]




## [0.7.1]  - 2018.07.20

* fipaste_pastery: durcissement si pas d'export et retour vide

## [0.7.0]  - 2018.07.3

* synchro: f__requis, f__sort_uniq

## [0.6.1] - 2018.06.28

* cosmetic: 
* added: f__sort_uniq
* synchro: fipaste_pastery
* syncro: f__basedirname
* syncro: f__requis
* new: fipaste_transfer pour fichier binaire < 100ko
* new : f__sudo
* rewriting: sudo lors traitement option, f_affichage
* rewriting: functions script_* 
* added: option -us, update en place
* fixed: correction en place/installé

## [0.4.0] - 2018.06.20

* initial commit, first public release 
* rewriting: fipaste_pastery

## [0.2.0] - 2018.02.11

## [0.1.0] - 2018.02.11
